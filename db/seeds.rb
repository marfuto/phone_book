

first = Contact.find_or_create_by!( email: "kitty@gmail.com") do |cat|
  cat.name =  "kitty"
end
first.numbers.create!(num: '098-304-555555', label: "Мобильный")

99.times do |n|
  mail = "catty-#{n+1}@gmail.com"
  Contact.find_or_create_by(email: mail) do |cat|
    cat.name = Faker::Name.name
    picture_number = (0..20).to_a.shuffle.first
    cat.picture = File.open(File.join(Rails.root,"app/assets/images/#{picture_number}.jpg"))
  end
end

contacts = Contact.order(:name).take(30).select {|c| c.numbers.count < 5}
1.times do
  contacts.each do |contact| 
    number = "098-#{(0..2).to_a.shuffle.join}-#{(0..8).to_a.shuffle.join}"
    contact.numbers.create!(num: number, label: "Мобильный")
  end
  
  
end

1.times do
   contacts.each do |contact| 
    number = "098-#{(0..2).to_a.shuffle.join}-#{(0..8).to_a.shuffle.join}"
    contact.numbers.create!(num: number, label: "Служебный")
  end
end