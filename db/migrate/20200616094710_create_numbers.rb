class CreateNumbers < ActiveRecord::Migration[5.2]
  def change
    create_table :numbers do |t|
      t.text :num
      t.text :label
      t.references :contact, foreign_key: true

      t.timestamps
    end
    add_index :numbers, [:contact_id, :created_at]
  end
end
