class AddPictureToContacts < ActiveRecord::Migration[5.2]
  def change
    add_column :contacts, :picture, :string
  end
end
