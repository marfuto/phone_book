require 'test_helper'

class NumbersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @contact = contacts(:toshiba)
    @number = numbers(:one)
  end

  test "should delete" do
    assert_difference 'Number.count', -1 do       
      delete number_path(@number)
    end
  end

  test "should create" do
    assert_difference 'Number.count', 1 do
      @contact.numbers.create!(num: '098-09-1243234', label: "Служебный")
    end
  end
end
