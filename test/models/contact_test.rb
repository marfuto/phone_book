require 'test_helper'

class ContactTest < ActiveSupport::TestCase
  def setup
    @contact = Contact.new(name: 'Example User', email: 'user@example.com')
  end

  test "should be valid" do
    assert @contact.valid?
  end

  test "name should be present" do
    @contact.name = "    "
    assert_not @contact.valid?
  end

  test "email should be present" do 
    @contact.email = "    "
    assert_not @contact.valid?
  end

  test "email addresses should be unique" do
    duplicate_contact = @contact.dup
    duplicate_contact.email = @contact.email.upcase
    @contact.save
    assert_not duplicate_contact.valid?
  end

  test "associated numbers should be destroyed" do
    @contact.save
    @contact.numbers.create!(num: "38-069-204205", label: "home")
    assert_difference 'Number.count', -1 do
      @contact.destroy
    end
  end
end
