require 'test_helper'

class NumberTest < ActiveSupport::TestCase
  def setup
    @contact = contacts(:toshiba)
    @number = @contact.numbers.build(num: "38-069-204205", label: "home")
  end

  test "should be valid" do
    assert @number.valid?
  end

  test "contact id should be present" do
    @number.contact_id = nil
    assert_not @number.valid?
  end

  test "num should be present" do
    @number.num = "   "
    assert_not @number.valid?
  end

end
