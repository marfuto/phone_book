require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title,         "Phone book"
    assert_equal full_title("Help"), "Help | Phone book"
  end
end