require 'test_helper'

class CreatingContactsTest < ActionDispatch::IntegrationTest
  def setup
    @contact = contacts(:toshiba)
  end
  test "invalid contact information" do
    get new_contact_path
    assert_no_difference 'Contact.count' do
      post contacts_path, params: {contact: { name:  "",
                               email: "user@invalid"}}
    end
    assert_template 'contacts/new'
  end

  test "valid contact information" do
    get new_contact_path
    assert_difference 'Contact.count', 1 do
      post contacts_path, params: {contact: { name:  "tratata",
                               email: "tratata@gmail.com"}}
    end
  end

  test "deleting contact" do
     assert_difference 'Contact.count', -1 do
      delete contact_path(@contact)
    end
  end
end
