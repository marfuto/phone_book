require 'test_helper'

class ContactsEditTest < ActionDispatch::IntegrationTest
   def setup
    @contact = contacts(:toshiba)
  end

  test "unsuccessful edit" do
    get edit_contact_path(@contact)
    assert_template 'contacts/edit'
    patch contact_path(@contact), params: {contact: { name:  "",
                                    email: "foo@invalid"} }
    assert_template 'contacts/edit'
  end

  test "successful edit" do
    get edit_contact_path(@contact)
    assert_template 'contacts/edit'
    name  = "shwarts"
    email = "shwarts@gmail.com"
    patch contact_path(@contact), params: {contact: { name:  name,
                                    email: email} }
    assert_not flash.empty?
    assert_redirected_to @contact
    @contact.reload
    assert_equal name,  @contact.name
    assert_equal email, @contact.email
  end
end
