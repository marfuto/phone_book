require 'test_helper'

class ContactsAllTest < ActionDispatch::IntegrationTest
 test "index including pagination" do
    get contacts_path
    assert_template 'contacts/contacts'
    assert_select 'div.pagination'
    Contact.paginate(page: 1).each do |contact|
      assert_select 'a[href=?]', contact_path(contact)
    end
  end
end
