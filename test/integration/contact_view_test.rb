require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper

  def setup
    @contact = contacts(:toshiba)
  end

  test "contact display" do
    get contact_path(@contact)
    assert_template 'contacts/show'
    assert_select 'title', full_title(@contact.name)
    assert_match @contact.name, response.body
    @contact.numbers.each do |number|
      assert_match number.num, response.body
    end
    assert_match @contact.email, response.body
  end
end