class Contact < ApplicationRecord
  has_many :numbers, dependent: :destroy
  before_save { self.email = email.downcase }
  mount_uploader :picture, PictureUploader
  default_scope {order('name')}
  validates :name, presence: true, length: { maximum: 100 },  uniqueness: true
  validates :email, presence: true, length: { maximum: 255 },
            uniqueness: { case_sensitive: false } 
   validate  :picture_size

   private

   def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end

end
