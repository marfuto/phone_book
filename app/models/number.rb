class Number < ApplicationRecord
  VALID_NUMBER = /[0-9]{1,3}-[0-9]{1,3}-[0-9]{1,10}/
  belongs_to :contact
  validates :contact_id, presence: true
  validates :num, presence: true, format: { with: VALID_NUMBER }
end
