class StaticPagesController < ApplicationController
  def help
  end

  def about
  end

  def contacts
  end
end
