class ContactsController < ApplicationController

  def show
    @contact = Contact.find(params[:id])
    @numbers = @contact.numbers.all
    @number = @contact.numbers.build
  end
  
  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)  
    if @contact.save
      flash[:success] = "Успешно создан!"
      redirect_to @contact
    else
      render 'new'
    end
  end

  def edit
    @contact = Contact.find(params[:id])
  end

  def update
    @contact = Contact.find(params[:id])
    if @contact.update_attributes(contact_params)
      flash[:success] = "контакт редактирован!"
      redirect_to @contact
    else
      render 'edit'
    end
  end

  def destroy
    Contact.find(params[:id]).destroy
    flash[:success] = "Контакт удален"
  end

  def contacts
    @contact = Contact.new
    @contacts = search.paginate(page: params[:page], :per_page   => 10)
  end

  def current_contact
    @current_contact = Contact.find(params[:id])
  end

  def search
    query = "#{params[:search]}"
    if /\d/.match(query)
      @contacts = Contact.joins(:numbers).where("num LIKE ?", "%#{query}%")
    else
      @contacts = Contact.where("name LIKE ?", "%#{query}%")
    end
  end

  private

    def contact_params
      param = params.require(:contact).permit(:name, :email, :picture)
      if param[:picture].nil?
        param[:picture] = default_picture
      end
      return param
    end

    def default_picture
      picture_number = (1..5).to_a.shuffle.first
      picture = File.open(File.join(Rails.root,"app/assets/images/defaultcat#{picture_number}.jpg"))
    end
end
