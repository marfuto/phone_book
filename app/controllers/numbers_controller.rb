class NumbersController < ApplicationController

  def create
    @contact = Contact.find(params[:contact])
    @number = @contact.numbers.create(number_params)
    if @number.save
      flash[:success] = "Успешно создан!"
      redirect_to @contact
    else
      flash[:success] = "неверно заполенное поле!"
      redirect_to @contact
    end
  end

  def destroy
    @number = Number.find(params[:id])
    @contact = Contact.find(@number.contact_id)
    @number.destroy
    # redirect_to @contact
  end

  private

  def number_params
    params.require(:number).permit(:num, :label)
  end
end
