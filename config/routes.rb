Rails.application.routes.draw do
  get 'contacts/new'
  root 'contacts#contacts'
  get 'help'    => 'static_pages#help'
  get 'about'   => 'static_pages#about'
  get 'contacts' => 'contacts#contacts'
  get 'newcontact' => 'contacts#new'
  get 'search' => 'contacts#search'
  resources :contacts
  resources :numbers,          only: [:create, :destroy]
end
